"use_strict";
let path = require("path");

/* Apparently it's bad for websites to require files directly from the disk, so this works as a middleman */

/* update to streaming maybe, who knows thats probably hard */
exports.giveFile = (req, res) => {
  let filename = req.params.name;
  let fileurl = path.resolve(__dirname + "/../files/" + filename);
  console.log("sending file: " + filename);
  res.sendFile(fileurl);
}
