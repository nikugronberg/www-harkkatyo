"use strict";

let mysql = require("mysql");

/* method for getting connection to the database */

let conn = mysql.createPool({
  connectionLimit: 9,
  host: "localhost",
  user: "soundground",
  password: "soundground",
  database: "soundground",
});

/*
conn.connect(function(err) {
  if(err) throw err;
  console.log("Connected to database!");
});
*/



exports.getConnection = () => {
  return conn;
}

