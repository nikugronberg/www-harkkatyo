"use strict";

let mysql = require("mysql");
let dbConnect = require("./dbConnect");
let ejs = require("ejs");
let fs = require("fs");

let conn = dbConnect.getConnection();

/* Get user input and give back the results as a html */
exports.find = (req, res) => {
  req.sanitizeBody("text").escape();
  req.sanitizeBody("text").trim();
  let text = req.body.text;

  /* don't send anything if there is no input from user */
  if(text.length == 0) {
    res.send({text: text});
    return;
  }
  /* get users and the amount of non-private uploads */
  let sql = "SELECT user.id AS id, user.name AS name, COUNT(audio.id) AS sounds FROM user LEFT JOIN audio ON audio.userid = user.id WHERE user.name LIKE ? AND (audio.isPublic = 1 OR audio.isPublic IS NULL) GROUP BY user.id ORDER BY LENGTH(user.name) ASC";

  conn.query(sql, ["%" + text + "%"], function(sqlerr, userResult) {
    if(sqlerr) {
      console.log(sqlerr);
    }
    /* get audio that is public */
    sql = "SELECT * FROM audio WHERE isPublic = 1 AND name LIKE ? AND url IS NULL ORDER BY LENGTH(name) ASC";
    conn.query(sql, ["%" + text + "%"], function(sqlerr, audioResult) {
      if(sqlerr) {
        console.log(sqlerr);
      }
      /* render html and send it */
      let fileurl = __dirname + "/../views/elements/findResults.ejs"
      ejs.renderFile(fileurl, {user: userResult, audio: audioResult}, {strict: true}, (err, html) => {
        if(err) throw err;
        res.send(html);

      });


    });
  });
}
