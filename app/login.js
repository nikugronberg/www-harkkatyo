"use strict";

let mysql = require("mysql");
let bcrypt = require("bcrypt");
let dbConnect = require("./dbConnect");


let conn = dbConnect.getConnection();

/* adding new user */
exports.signup = function (req, res) {
  /* do some cleaning, if errors (which there shouldn't be since client side js should handle it) refresh the page */
  req = checkAndSanitize(req);

  let errors = req.validationErrors();
  if(errors) {
    console.log(errors);
    res.render("pages/signup");
  } else {
    /* hash the password and put the user in database*/
    bcrypt.hash(req.body.password, 10, function(err, hash) {
      let user = {
        name: req.body.username,
        password: hash,
      };
      let sql = "INSERT INTO user SET ?";
      conn.query(sql, user, function(err, result) {
        if(err) {
          console.log(err);
          /* most likely error that actually happens, but this is pretty bad design to assume the error */
          console.log("Tried to create dublicate user: " + user.name);
          res.render("pages/signup", {message: "User already exists"});
        } else {
          console.log("New user added succesfully");
          //log user in
          req.session.user = req.body.username;
          req.session.userid = result.insertId;

          /* redirect the user to their new profile page */
          res.redirect("/user/" + user.name);
        }
      });
    });

  }
}

/* user login funvtion */
exports.login = function (req, res) {
  /* cleaning up */
  req.sanitizeBody("username").escape();
  req.sanitizeBody("username").trim();
  req.sanitizeBody("password").escape();

  let errors = req.validationErrors();
  if(errors) {
    console.log(errors);
    res.render("pages/login");
  } else {
    let user = {
      name: req.body.username,
      password: req.body.password,
    };
    /* get the user from the database */
    let sql = "SELECT * FROM user WHERE name = ?";
    conn.query(sql, user.name, function(err, result, fields) {
      if(err) {
        throw err;
        return;
      } else if(result[0]) {
        /* compare passwords */
        bcrypt.compare(user.password, result[0].password, function(err, hashResult) {
          if(hashResult) {
            req.session.user = user.name;
            req.session.userid = result[0].id;
            console.log("User logged in succesfully");
            res.redirect("/user/" + user.name);
          } else {
            /* here we could differentiate between wrong password and wrong username */
            res.render("pages/login",{message: "Wrong username or password"});
          }
        });
      } else {
        res.render("pages/login",{message: "Wrong username or password"});
      }
    });

  }
}

/* logout, happens by destroying session variable */
exports.logout = function(req, res) {
  req.session.destroy();
  res.redirect("/");
}

/* do some cleaning up, can't trim password, because it can contain spaces at the end */
const checkAndSanitize = (req) => {
  req.checkBody("username", "Invalid name").isAlphanumeric().notEmpty();
  req.checkBody("password", "Invalid password").isAlphanumeric().notEmpty();

  req.sanitizeBody("username").escape();
  req.sanitizeBody("username").trim();
  req.sanitizeBody("password").escape();
  return req;
}
