"use strict";

let mysql = require("mysql");
let dbConnect = require("./dbConnect");

let conn = dbConnect.getConnection();

/* handles nouser links to get the audio from sql */
exports.page = (req, res) => {
  let url = req.params.url;

  let sql = "SELECT * FROM audio WHERE url = ?";
  conn.query(sql, url, function(sqlerr, result) {
    if(sqlerr) {
      console.log(sqlerr);
    }
    res.render("pages/nouserPage", {audio: result[0]});
  });
}
