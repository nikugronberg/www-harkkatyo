"use strict";

/* router is basically front controller */

let express = require("express");
let path = require("path");
let login = require("./login");
let upload = require("./upload");
let user = require("./user");
let nouser = require("./nouser");
let audioServer = require("./audioServer");
let find = require("./find");

let router = express.Router();

/* export the router to server */
module.exports = router;

/* ROUTES */
/* startpage, show different page depending on if the user is logged in or not */
router.get('/',function(req,res){
    if(req.session.user) {
      res.render("pages/home");
    } else {
      res.render("pages/index");
    }
});

/* logout */
router.get("/logout", login.logout);

/* signup */
router.get('/signup',function(req,res){
    res.render("pages/signup");
});
router.post("/signup", login.signup);

/* login */
router.get('/login',function(req,res){
    res.render("pages/login");
});
router.post("/login", login.login);

/* find */
router.get('/find',function(req,res){
    res.render("pages/find");
});
router.post("/find", find.find);

/* upload */
router.get('/upload',function(req,res){
    res.render("pages/upload");
});
router.post("/upload", upload.fileDownload);
router.post("/upload.info", upload.fileInfo);
router.post("/upload.cancel", upload.fileCancel);
router.post("/upload.waveform", upload.waveform);

/* user page */
router.get("/user/:username", user.page);
router.post("/user.delete", user.delete);

/* no user uploads */
router.get("/nouser/:url", nouser.page);

/* sending audio files and maybe other files too */
router.get("/:something?/files/:name", audioServer.giveFile);
