"use strict";

/* we need a lot of things here */
let formidable = require("formidable");
let fs = require("fs");
let mysql = require("mysql");
let dbConnect = require("./dbConnect");
let filetype = require("file-type");
let readChunk = require("read-chunk");
let md5 = require("md5");
let ejs = require("ejs");


let conn = dbConnect.getConnection();

/* user uploads sound */
exports.fileDownload = (req, res) => {
  console.log("Starting upload");
  let form = new formidable.IncomingForm();
  /* put things in temp directory first */
  form.uploadDir = "./temp";
  form.type = true;
  /* create audio as well as we can at this point */
  form.parse(req, function(err, fields, files) {
    let audio = {
      //id:,
      userid: req.session.userid ? req.session.userid : 0,
      fileurl: "not set",
      name: files.filetoupload.name,
      author: req.session.user ? req.session.user : "ID",

      //date:
      plays: 0,
      likes: 0,
      download: 0,
      isPublic: 1,
    }

    /* check filetype == mp3 if not, send message to user*/
    let buffer = readChunk.sync(files.filetoupload.path, 0, 4100);
    if(filetype(buffer).ext != "mp3") {
      console.log("Not an mp3 file, upload cancelled");

      fs.unlink(files.filetoupload.path, function(err) {
        if(err) throw err;
      });

      res.status(406).send("Sorry, this site only accepts .mp3 files");
      return;
    }

    /* insert the audio first time to get its id */
    let sql = "INSERT INTO audio SET ?";
    conn.query(sql, audio, function(sqlerr, result) {
      if(sqlerr) {
        //throw err;
        console.log(err);
      } else {
        audio.id = result.insertId;

        /* move it to the main "files" folder */
        let oldpath = files.filetoupload.path;
        audio.fileurl = './files/' + files.filetoupload.name + "-" + (audio.id).toString();
        fs.rename(oldpath, audio.fileurl, function (err) {
          if (err) throw err;

        });
        console.log("Upload complete");
        /* send audio metadata back, so user can change its values */
        res.send(audio);
      }
    });
  });
}

/* here we get the metadata back and store it to database */
exports.fileInfo = (req, res) => {
  if(!req.session.user) {
    /* if user is not logged in, we create md5 hash to be the url for the upload */
    req.body.url = md5(req.body.fileurl);
    //console.log(req.body.url);
  }
  /* update database entry */
  let sql = "UPDATE audio SET ? WHERE id = ?";
  conn.query(sql, [req.body, req.body.id], function(sqlerr, result) {
    if(sqlerr) {
      console.log(sqlerr);
    }

    /* the link that takes user to their profile/private nonlogged upload */
    let fileurl = __dirname + "/../views/elements/uploadLink.ejs"
    ejs.renderFile(fileurl, {audio: req.body, user: req.session.user}, {strict: true}, (err, html) => {
      if(err) throw err;
      res.send(html);
    });
  });
}

/* if user cancels or leaves page before saving we delete all the files and remove sql entry */
exports.fileCancel = (req, res) => {
  console.log("Upload cancelled by user");
  let sql = "DELETE FROM audio WHERE id = ?";
  conn.query(sql, req.body.id, function(sqlerr, result) {
    if(sqlerr) {
      console.log(sqlerr);
    }
  });
  fs.unlink(req.body.fileurl, function(err) {
    if(err) throw err;
  });
}

/* the waveform data to create the visuals, we store that and also the duration of the audio */
/* this way we don't need to send the mp3 to get the visuals or duration to the client */
/* its really helpful when there are a lot of wavesurfers in the page */
exports.waveform = (req, res) => {
  let array = [];
  array = req.body.array;
  let fileurl = req.body.fileurl + ".dat";
  //console.log(fileurl);
  /* save the .dat to a file */
  fs.writeFile(fileurl, array, (err) => {
    if(err) throw err;
  });
  /* put the duration to the database */
  let sql = "UPDATE audio SET ? WHERE id = ?";
  conn.query(sql, [{duration: req.body.duration}, req.body.id], function(sqlerr, result) {
    if(sqlerr) {
      console.log(sqlerr);
    }
  });

}
