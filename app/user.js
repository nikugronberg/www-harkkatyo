"use strict";

let mysql = require("mysql");
let dbConnect = require("./dbConnect");
let fs = require("fs");



let conn = dbConnect.getConnection();

exports.page = (req, res) => {
  let username = req.params.username;

  /* get user info from database */
  let sql = "SELECT * FROM user WHERE name = ?";
  conn.query(sql, username, function(err, result) {
    if(err) {
      console.log(err);
    } else if (!result[0]) {
      /* user not found, do something about it */
      res.status(404).send("<h2>404</h2><p>Yup, you have wandered into unknown territory, please come back to Soundground</p>")
    } else {
      let admin = false;
      let sql;
      if(username == req.session.user) {
        /* user is in his/her homepage, show all tracks */
        sql = "SELECT * FROM audio WHERE userid = ? ORDER BY datetime DESC";
        admin = true;
      } else {
        /* someone else's page, don't show hidden stuff */
        sql = "SELECT * FROM audio WHERE userid = ? AND isPublic = 1 ORDER BY datetime DESC";
        admin = false;
      }

      conn.query(sql, result[0].id, function(err, result2) {
        if(err) {
          console.log(err);
        }
        res.render("pages/userPage", {user: result[0], audio: result2, admin: admin});
      });
    }
  });
}

/* user deletes audio here */
exports.delete = (req, res) => {
  let fileurl = req.body.fileurl;
  console.log("User deleted: " + fileurl);

  /* remove from sql */
  let sql = "DELETE FROM audio WHERE fileurl = ?";
  conn.query(sql, fileurl, function(sqlerr, result) {
    if(sqlerr) {
      console.log(sqlerr);
    }
    /* deleting the files, .mp3 and .dat files */
    fs.unlink(fileurl, function(err) {
      if(err) console.log(err);
    });
    fs.unlink(fileurl + ".dat", function(err) {
      if(err) console.log(err);
    });

    res.end();
  });
}
