"use strict";

/* post user input and if we get stuff back initialize the players/wavesurfers */

$(document).ready(() => {
  let func;
  $("#findText").on("input", () => {
    /* only post if user has not typed in the last 0.5s */
    let delay = 500;

    if(func) {
      clearTimeout(func);
      func = null;
    }

    func = setTimeout( () => {
      $.post("/find", {text: $("#findText").val()}, (res) => {
        if(res.length) {
          /* clear results and append new ones */
          $(".results").empty();
          $(res).appendTo(".results");
          initPlayers();
        }
      });
    }, delay);
  });
});
