"use strict";

/* validate forms, this pretty much explains itself */
/* we have rules and messages if rules are not met */
$(document).ready(function() {


  $("form").validate({
    rules: {
      username: {
        required: true,
        minlength: 1,
        maxlength: 32,
        alphanumeric: true,
      },
      password: {
        required: true,
        minlength: 4,
        maxlength: 256,
        alphanumeric: true,
      },
    },
    messages: {
      username: {
        required: "Enter a username",
        minlength: "Username needs to be atleast 3 characters",
        maxlength: "Username needs to be under 32 characters",
        alphanumeric: "Username can't contain special characters",
      },
      password: {
        required: "Enter a password",
        minlength: "Password needs to be atleast 4 characters",
        maxlength: "Password needs to be under 256 characters",
        alphanumeric: "Password can't contain special characters",
      }
    },
    errorPlacement: (error, element) => {
      error.insertAfter(element);
    },
    errorClass: "warning",

  });

});
