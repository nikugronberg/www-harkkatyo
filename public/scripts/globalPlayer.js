"use strict";

$(document).ready(() => {
  /* show volume slider if we click the button */
  $("header").on("click", "#dropbutton", (event) => {
    $("header").find("#dropmenu").toggle("hide");

  });

  /* hide volume slider if we click on something else */
  $(document).on("click", (event) => {
    if($("header").find("#dropmenu").is(":visible") && !$.contains($("header").find(".dropdown").get(0), event.target)) {
      $("header").find("#dropmenu").toggle("hide");
    }
  });

  /* if we have active wavesurfer, make play button work */
  $("header").on("click", "#globalPlayPause", () => {
    if(typeof active !== "undefined") {
      if(active.isPlaying()) {
        active.pause();
      } else {
        /* stop others players from playing */
        for(let i=0; i<wavesurfers.length; i++) {
          wavesurfers[i].pause();
        }
        /* play the active */
        active.play();
      }
    }
  });


});
