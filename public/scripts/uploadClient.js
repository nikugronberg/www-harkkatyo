"use strict";

/* some global variables */
let id;
let fileurl;
let saved = false;
let waveformarray;
let duration;
let wavesurfer;

/* detect when user picks a file and automatically upload it */
$(document).ready(function() {
  $("#fileUpload").change(function() {

    upload();
  });

/* save button generates metadata from form and sends that along waveformdata to the server */
  $("#save").on("click", function() {
    let form = generateForm();
    /* change saved to true so user can leave page without having their upload deleted */
    saved = true;
    $.post("/upload.waveform",  {array: waveformarray, fileurl: fileurl, duration: duration, id: id});
    $.post("/upload.info", form, (res) => {
      if(res.length) {
        /* we get the link as a response, we append that to the page */
        $("#link").empty();
        $(res).appendTo("#link");
      }
    });
  });

  /* cancel the upload, delete everything, user can upload new file */
  $("#cancel").on("click", function() {
    let form = generateForm();
    $("#fileUpload").prop("disabled", false);
    $(".uploadInfo").toggle();
    wavesurfer.destroy();
    $.post("/upload.cancel", form);
  });

  /* if user leaves page after selecting a file and before saving, we delete the whole thing(upload) */
  $(window).on("unload", function() {
    if(!saved && $("#fileUpload").prop("disabled")) {
      let form = generateForm();
      $.post("/upload.cancel", form);
    }
  });
});


/* using ajax to upload the mp3 */
const upload = () => {
  let formData = new FormData($("#uploadForm")[0]);
  $.ajax({
    method: "POST",
    processData: false,
    contentType: false,
    async: true,
    cache: false,
    data: formData,
    success: function(res) {
      /* when the upload succeeds, show the form to change metadata, also put placeholders in the form */
      $("#fileUpload").prop("disabled", true);
      $(".uploadInfo").toggle();
      $("#uploadForm").find(".warning").text("");
      id = res.id;
      fileurl = res.fileurl;
      $("input[name=name]").val(res.name.slice(0, res.name.lastIndexOf("."))); /* remove ".mp3" */
      $("input[name=author]").val(res.author);
      $("input[name=download]").prop("checked", res.download == 1);
      $("input[name=isPublic]").prop("checked", res.isPublic == 1 && res.userid != 0);

      /* generate the waveformdata file and send it to server */
      /* this way user generates the waveform, so we spare some server resources */
      /* probably not that safe though */
        wavesurfer = WaveSurfer.create({
        container: "#waveform",
        waveColor: "darkgray",
        progressColor: "gray",
        barWidth: 5,
        height: 50,
      });
      wavesurfer.load(fileurl);
      wavesurfer.on("ready", function () {
        waveformarray = exportPCMFixed(wavesurfer, 128, 1000, true, 0);
        duration = wavesurfer.getDuration();
        /* after all is done, we can enable the save button */
        $("#save").prop("disabled", false);
      });
    },
    error: function(res) {
      /* on error, show error */
      /* mainly for wrong kind of file uploads */
      $("#uploadForm").find(".warning").text(res.responseText);
    },
  });
}

/* generates form that has all the useful information that we need back at the server */
const generateForm = () => {
  let form = {
    id: id,
    name: $("input[name=name]").val().toString(),
    author: $("input[name=author]").val().toString(),
    fileurl: fileurl,
    isPublic: $("input[name=isPublic]").prop("checked") ? 1 : 0,
    download: $("input[name=download]").prop("checked") ? 1 : 0,
  }
  return form;
}

/* wavesurfers vanilla exportPCM was broken so I fixed it */
/* basically this generates json file that has the peaks (length is the amount of peaks) of the audio */
const exportPCMFixed = (wavesurfer, length, accuracy, noWindow, start) => {
      length = length || 1024;
      accuracy = accuracy || 10000;
      noWindow = noWindow || false;
      start = start || 0
      const peaks = wavesurfer.backend.getPeaks(length, start, length - 1);
      const arr = [].map.call(peaks, val => Math.round(val * accuracy) / accuracy);
      const json = JSON.stringify(arr);
      if (!noWindow) {
          window.open('data:application/json;charset=utf-8,' +
              encodeURIComponent(json));
      }
      return json;
}
