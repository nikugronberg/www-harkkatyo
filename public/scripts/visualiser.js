"use strict";

/* wavesurfer == player, it's the thing that plays the audio, both words are used in comments */
/* list of all players on that page and the active wavesurfer */
let wavesurfers = [];
let active;

/* init players */

$(document).ready( () => {
  initPlayers();
});




const initPlayers = () => {
  /* clear old wavesurfers if they exist */
  if(wavesurfers.length) {
    for(let i=0; i<wavesurfers.length; i++) {
      wavesurfers[i].destroy();
    }
  }
  /* create new wavesurfers */
  $(".waveform").each(function(index, element) {
      let wavesurfer = WaveSurfer.create({
      container: element,
      waveColor: "darkgray",
      progressColor: "gray",
      cursorWidth: 3,
      renderer: "MultiCanvas",
      barHeight: 1,
      barWidth: 5,
      height: 64,
    });
    wavesurfers.push(wavesurfer);
    wavesurfer.setVolume($("#globalVolume").val() / $("#globalVolume").prop("max"));

    /* parse sql date into something reasonable */
    let date = $(element).find(".date").text().slice(0, 24);
    $(element).find(".date").text(dateFormat(date, "dd.mm.yyyy"));




    /* waveform visuals and the actual audio file */
    /* audio is not loaded until the wavesurfer is played */
    let fileurl = $(this).data("file");
    $.get(fileurl + ".dat", function(peaks) {
      let json = JSON.parse(peaks);
      wavesurfer.load(fileurl, json, "metadata");
    });

    /* BUTTONS */
    /* playe/pause button */
    $(element).on("click", "#playPause", () => {
      if(wavesurfer.isPlaying()) {
        wavesurfer.pause();
      } else {
        /* stop others players from playing */
        for(let i=0; i<wavesurfers.length; i++) {
          wavesurfers[i].pause();
        }
        wavesurfer.play();
      }
      /* to fix random problems, there are a lot of play() */
      wavesurfer.on("ready", () => {
        play($(this), wavesurfer);
      });
      play($(this), wavesurfer);
    });

    /* stop button */
    $(element).on("click", "#stop", () => {
      $(this).find("#playPause").find("img").prop("src", "/icons/play.svg");
      wavesurfer.stop();
      updateTimeAndPosition($(this), wavesurfer.getCurrentTime(), wavesurfer.getDuration());
    });

    /* delete dropdown menu button */
    $(element).on("click", "#dropbutton", () => {
      $(this).find("#dropmenu").toggle("hide");
    });

    /* delete dropdown options */
    $(element).on("click", ".option", (event) => {
      if($(event.target).text() == "Delete") {
        $.post("/user.delete", {fileurl: fileurl}, () => {
          location.reload();
        });
      } else {
        $(this).find("#dropmenu").toggle("hide");
      }
    });

    /* if dropdown is open and we click somewhere else this closes the dropdown */
    $(document).on("click", (event) => {
      if($(element).find("#dropmenu").is(":visible") && !$.contains($(element).find(".dropdown").get(0), event.target)) {
        $(element).find("#dropmenu").toggle("hide");
      }
    });

    /* OTHER STUFF, LIKE EVENTS AND GLOBAL PLAYER STUFF */
    $("#globalVolume").on("input", () => {
      wavesurfer.setVolume($("#globalVolume").val() / $("#globalVolume").prop("max"));
    });

    /* select audio position from the global position slider */
    $("#globalPosition").on("change", () => {
      if(wavesurfer.isPlaying()) {
        /* -0.001 fixes issue of restarting song when sliding all the way to the end */
        wavesurfer.seekTo($("#globalPosition").val() / $("#globalPosition").prop("max") - 0.001);
      }
    });

    /* updates the time counters and global slider position, audioprocess fires continuosly as the wavesurfer is playing */
    wavesurfer.on("audioprocess", () => {
      updateTimeAndPosition($(this), wavesurfer.getCurrentTime(), wavesurfer.getDuration());
    });

    /* update buttons when play event happens, also make this the active wavesurfer */
    wavesurfer.on("play", () => {
      $(this).find("#playPause").find("img").prop("src", "/icons/pause.svg");
      $("#globalPlayPause").find("img").prop("src", "/icons/pause.svg");
      active = wavesurfer;
      play($(this), wavesurfer);
    });

    /* update buttons on pause event */
    wavesurfer.on("pause", () => {
      $(this).find("#playPause").find("img").prop("src", "/icons/play.svg");
      $("#globalPlayPause").find("img").prop("src", "/icons/play.svg");
    });

    /* do things when audio ends depending on the global choice (repeat/next/stop) */
    wavesurfer.on("finish", () => {
      if($("#globalEndOption option:selected").val() == "repeat") {
        wavesurfer.play();
        /* It seems like wavesurfer fires pause event when restarting the audio, but not play event for some reason */
        /* This here is a fix for the buttons that rely on those events to work correctly */
        /* we unsubscribe from the pause event and when we get audioprocess event, we resub to the pause event */
        wavesurfer.un("pause");
        wavesurfer.on("audioprocess", () => {
          wavesurfer.on("pause", () => {
            $(this).find("#playPause").find("img").prop("src", "/icons/play.svg");
            $("#globalPlayPause").find("img").prop("src", "/icons/play.svg");
          });
        });
      /* play next */
      } else if ($("#globalEndOption option:selected").val() == "next") {
        let index = wavesurfers.indexOf(wavesurfer);
        /* if there is next wavesurfer, change to that, otherwise play the first one */
        if(index + 1< wavesurfers.length) {
          wavesurfers[index + 1].play();
        } else {
          wavesurfers[0].play();
        }
      } else {
        /* stop, but it happens automatically, we don't need to do anything here */
      }
    });

    /* set duration text for this wavesurfer */
    let duration = secondsToMinutes($(element).find("#endTime").text());
    $(element).find("#endTime").text(duration);

  });
}


/* set volume on play and update global player's names */
const play = (element, wavesurfer) => {
  if(wavesurfer.isPlaying()) {
    wavesurfer.setVolume($("#globalVolume").val() / 100);
    /* names of the author and sound are limited to 15 chars. */
    /* This isn't the best solution, but it fixes some problems with the navbar */
    $("#globalName").text(element.find(".title").text().slice(0,15));
    $("#globalAuthor").text(element.find(".author").text().slice(0,15));
  }
}

/* set the current time, global times and global position slider */
const updateTimeAndPosition = (element, current, duration) => {
  let time = secondsToMinutes(current);
  element.find("#currentTime").text(time);
  $("#globalCurrentTime").text(time);
  $("#globalEndTime").text(secondsToMinutes(duration));
  /* if user is not interacting with the position slider, then update its position */
  if(!$("#globalPosition:active").length) {
    $("#globalPosition").val(Math.round(current/duration * $("#globalPosition").prop("max")));
  }

}

/* convert seconds to something more pleasing to read, format "m:ss" */
const secondsToMinutes= (sec) => {
  let minutes = Math.floor(sec / 60);
  let seconds = Math.floor(sec - minutes * 60);
  seconds < 10 ? seconds = ("0" + seconds) : seconds;
  return minutes + ":" + seconds;
}
