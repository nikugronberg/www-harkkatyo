"use strict";

/* requiring stuff */
let express = require("express");
let app = express();
let server = require("http").Server(app);
let session = require("express-session");
let expressLayouts = require("express-ejs-layouts");
let bodyParser = require("body-parser");
let validator = require("express-validator");

/* use ejs layouts */
app.set("view engine", "ejs");
app.use(expressLayouts);

/* raise the limits for files */
app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({limit: "50mb", extended: true }));
app.use(validator());

/* initialize session variable */
app.use(session({
  secret: "shrek is love shrek is life",
  resave: false,
  saveUninitialized: true,
}));

//give session variable to ejs layout
app.use(function(req, res,next) {
  res.locals.session = req.session;
  next();
});

//router
let router = require("./app/routes");
app.use("/", router);


//directories
app.use(express.static(__dirname + '/public'));

//start the server
server.listen(process.env.PORT || 7001); // Listens to port 7001
console.log('Listening on ' + server.address().port);
