-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 15, 2017 at 02:34 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `soundground`
--

-- --------------------------------------------------------

--
-- Table structure for table `audio`
--

CREATE TABLE `audio` (
  `id` int(11) NOT NULL,
  `fileurl` varchar(512) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `name` varchar(256) NOT NULL,
  `author` varchar(256) NOT NULL,
  `duration` float NOT NULL DEFAULT '0',
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `plays` int(11) NOT NULL DEFAULT '0',
  `likes` int(11) NOT NULL DEFAULT '0',
  `download` tinyint(1) NOT NULL DEFAULT '0',
  `isPublic` tinyint(1) NOT NULL DEFAULT '1',
  `url` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `audio`
--

INSERT INTO `audio` (`id`, `fileurl`, `userid`, `name`, `author`, `duration`, `datetime`, `plays`, `likes`, `download`, `isPublic`, `url`) VALUES
(140, './files/MidgetBar.mp3-140', 11, 'Midget Bar', 'asa', 106.867, '2017-12-13 23:03:53', 0, 0, 1, 1, NULL),
(141, './files/lofi beats.mp3-141', 11, 'lofi beats', 'asa', 100.911, '2017-12-13 23:09:20', 0, 0, 0, 1, NULL),
(142, './files/Slender game music idea.mp3-142', 11, 'My Epic Orchestral', 'asa', 4.04898, '2017-12-13 23:09:33', 0, 0, 1, 0, NULL),
(143, './files/horrible sound.mp3-143', 21, 'horrible sound', 'Wew', 1.64571, '2017-12-13 23:11:19', 0, 0, 0, 1, NULL),
(144, './files/Random 8bit.mp3-144', 21, 'Random 8bit', 'Wew', 14.1584, '2017-12-13 23:11:36', 0, 0, 1, 1, NULL),
(147, './files/Orchestal lullaby.mp3-147', 25, 'Ogres are like onions, they have layers', 'Shrek', 56.1371, '2017-12-13 23:20:44', 0, 0, 1, 1, NULL),
(150, './files/horrible sound.mp3-150', 0, 'horrible sound', 'ID', 1.64571, '2017-12-14 01:21:22', 0, 0, 0, 0, 'a809dc76b6fb19e01b8458975f3fcf0d'),
(151, './files/Orchestal lullaby.mp3-151', 0, 'Shrek\'s tears', 'Onion', 56.1371, '2017-12-14 12:57:56', 0, 0, 0, 0, '9821db0254f4308a78f9b1e59e6bb0ec');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `password` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `password`) VALUES
(4, 'hashman', '$2a$10$NCy52mJmqvz0IjkcMu5k5OLpAM1DTsgN7ZrDKOKYTExU4YCE7ndXW'),
(5, 'a', '$2a$10$UeVDB6GYYAs05LWUc5qLyeX0tIMlNHDK2UXSL.vXNWZSv4dHwJkaO'),
(6, 'b', '$2a$10$O.yLgUIp7CufcOK/J.aKseEaeqbVMPpHy3iQyyYiar.asX.EPXbPC'),
(7, 'c', '$2a$10$m5x5uyjOsJ4/H4v2Wck4EOtzKiquaNil6Wmjc/5g5T6cc4zFVOYTy'),
(9, 'q', '$2a$10$wSS9f.78S6IqF3sj/7g4j.jstBSwXHtaztbOcRYJcptiTBbM81xYG'),
(10, 'w', '$2a$10$ZgUDWqsOanTL5SaAOjzhduDFKLHcKrDB6UpNTn51UiOsHdRtrD4vC'),
(11, 'asa', '$2a$10$uhAmCTTM7R8RaDZ6A8E.AO.QekwYJ5RebveY3tAOkdNM/LaLLnuza'),
(18, 'asaa', '$2a$10$u5pvHw5E9QY2mxYwSJdUwuMInU8wV1unJW3kWqVxqnEsD2W32kX.i'),
(19, 'Jani', '$2a$10$peTAWLB9.9taKFE0npVUbOVm3T1kHjTEP1R9kLAofqOZ58OHn.9ZK'),
(21, 'Wew', '$2a$10$LSlSFER50upm2JmQP23NbuIX.hsAU7A3xkTk16QNOnDYj.RNn/FbW'),
(23, 'dad', '$2a$10$PGhAXVpDtVB1JxpsxAGGEeGEQStbphUuqW6fXVrL5TGuzl/EUvjfK'),
(24, '123', '$2a$10$nVL4NrCg/CdOcZGnf/Vdw.lxxUOBv6PaAJvesMfyu.Hnfs60bqFTW'),
(25, 'Shrek', '$2a$10$watdjLl2HXw.wEBpt7EhHOJE9S3ohzoTe1BqVKP./t/ie3vz73tC.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `audio`
--
ALTER TABLE `audio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `name_2` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `audio`
--
ALTER TABLE `audio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
